import Airplane
import Runway


def test_one():
    """
    This test will be used to check the insert_planes function of the Runway class. I want to see if the correct planes
    are being added and if the invalid planes are being left out. Also to check if the earliest submission time is
    actually returned by the code.
    :return: Nothing
    """
    print("----------TEST 1----------")
    print("Expected Output:")
    print("SouthWest 10 not added! Not a valid plane! Request time is before submission time!")
    print("['SouthWest 45', '0', '6', 7]")
    print("['United 4', '0', '6', 9]")
    print("['United 174', '7', '7', 11]")
    print("['SouthWest 125', '8', '9', 11]")
    print("output = 0")
    test_one_runway = Runway.Runway()
    output = test_one_runway.insert_planes("Test1.txt")
    # There should only be four planes because 1 is not a valid plane.
    print("Output:")
    for plane in test_one_runway.planepen:
        print(plane.get_airplane())
    print("output = " + str(output))
    print("\n")


def test_two():
    """
    This test is to make sure that the correct format for the state strings is returned to the terminal
    :return: Nothing
    """
    print("----------TEST 2----------")
    test_two_runway = Runway.Runway()
    airplane1 = Airplane.Airplane("Allegiant 33", 1, 2, 4)
    airplane2 = Airplane.Airplane("United 127", 1, 5, 6)
    airplane3 = Airplane.Airplane("Spirit 125", 5, 7, 3)
    test_two_runway.runway.append(airplane1)
    test_two_runway.runway.append(airplane2)
    test_two_runway.runway.append(airplane3)
    # Test to see if current time before start plane it would say schedule instead of start.
    print("Expected Output:")
    print("At time 0 the queue would look like: Allegiant 33(Scheduled for 2), United 127(Scheduled for 5), Spirit 125(Scheduled for 7)")
    print("Output:")
    print(test_two_runway.check_runway(0))
    print("\n")
    # Test to see if time has passed and see if the first plane is starting.
    print("Expected Output:")
    print("At time 3 the queue would look like: Allegiant 33(Started at 2), United 127(Scheduled for 5), Spirit 125(Scheduled for 7)")
    print("Output:")
    print(test_two_runway.check_runway(3))
    print("\n")
    # Same two tests but for solo plane so pop last two planes
    test_two_runway.runway.pop(2)
    test_two_runway.runway.pop(1)
    print("Expected Output:")
    print("At time 0 the queue would look like: Allegiant 33(Scheduled for 2)")
    print("Output:")
    print(test_two_runway.check_runway(0))
    print("\n")
    # Test to see if first time has passed and see if the first plane is starting.
    print("Expected Output:")
    print(test_two_runway.check_runway(3))
    print("Output:")
    print("At time 3 the queue would look like: Allegiant 33(Started at 2)")
    print("\n")


def test_three():
    """
    This test is to see if the correct planes are being added to the runway queue when the time has come.
    :return: Nothing
    """
    print("----------TEST 3----------")
    test_three_runway = Runway.Runway()
    airplane1 = Airplane.Airplane("Allegiant 33", 1, 2, 4)
    airplane2 = Airplane.Airplane("United 127", 1, 5, 6)
    airplane3 = Airplane.Airplane("Spirit 125", 5, 7, 3)
    test_three_runway.planepen.append(airplane1)
    test_three_runway.planepen.append(airplane2)
    test_three_runway.planepen.append(airplane3)
    # Test to see if only planes that submitted at time 1 are added
    print("Expected Output:")
    print("['Allegiant 33', 1, 2, 5]")
    print("['United 127', 1, 5, 10]")
    print("Output:")
    test_three_runway.add_submissions(1)
    for plane in test_three_runway.runway:
        print(plane.get_airplane())
    print("\n")
    # Test to see as time 5 rolls in the new plane is added.
    print("Expected Output:")
    print("['Allegiant 33', 1, 2, 5]")
    print("['United 127', 1, 5, 10]")
    print("['Spirit 125', 5, 7, 9]")
    print("Output:")
    test_three_runway.add_submissions(5)
    for plane in test_three_runway.runway:
        print(plane.get_airplane())
    print("\n")


def test_four():
    """
    This test is for making sure that a plane can be added into the queue and that it will sort and update the correct
    order. This test also makes sure that planes that are being pushed back get their start and end times changed.
    :return: Nothing
    """
    print("----------TEST 4----------")
    test_four_runway = Runway.Runway()
    airplane1 = Airplane.Airplane("United 127", 1, 8, 6)
    airplane2 = Airplane.Airplane("Allegiant 33", 1, 2, 4)
    test_four_runway.runway.append(airplane1)
    test_four_runway.runway.append(airplane2)
    test_four_runway.organize_planes()
    # Adding two planes that have same sub time but different req time.
    # Allegiant added last but should be first in queue
    print("Expected Output:")
    print("['Allegiant 33', 1, 2, 5]")
    print("['United 127', 1, 8, 13]")
    print("Output:")
    for plane in test_four_runway.runway:
        print(plane.get_airplane())
    print("\n")
    # Add new plane between the two previous planes
    # Should change order and new start and end times assigned.
    airplane3 = Airplane.Airplane("Spirit 125", 5, 7, 3)
    test_four_runway.runway.append(airplane3)
    print("Expected Output:")
    print("['Allegiant 33', 1, 2, 5]")
    print("['Spirit 125', 5, 7, 9]")
    print("['United 127', 1, 8, 13]")
    print("Output:")
    test_four_runway.organize_planes()
    for plane in test_four_runway.runway:
        print(plane.get_airplane())
    print("\n")


def test_five():
    """
    This test is for testing the system as a whole and read in a file.
    Some things that will be tested are the order of the planes and when the should fly
    Gaps within the list of planes when there are no submissions
    Checking to make sure planes are added in by their priority.
    Testing return output of simulator function.
    :return: Nothing
    """
    print("----------TEST 5----------")
    runway = Runway.Runway()
    starttime = runway.insert_planes("Test5.txt")
    print("Expected Output:")
    print("At time 0 the queue would look like: JetBlue 113(Scheduled for 1), Alaska 179(Scheduled for 4)")
    print("At time 1 the queue would look like: JetBlue 113(Started at 1), United 125(Scheduled for 4), Alaska 179(Scheduled for 6)")
    print("At time 2 the queue would look like: JetBlue 113(Started at 1), United 125(Scheduled for 4), Alaska 179(Scheduled for 6)")
    print("At time 3 the queue would look like: JetBlue 113(Started at 1), United 125(Scheduled for 4), Alaska 179(Scheduled for 6)")
    print("At time 4 the queue would look like: United 125(Started at 4), Alaska 179(Scheduled for 6)")
    print("At time 5 the queue would look like: United 125(Started at 4), Alaska 179(Scheduled for 6)")
    print("At time 6 the queue would look like: Alaska 179(Started at 6)")
    print("At time 7 the queue would look like: No planes taxiing!")
    print("At time 8 the queue would look like: United 147(Scheduled for 12)")
    print("At time 9 the queue would look like: SouthWest 25(Scheduled for 10), United 147(Scheduled for 15)")
    print("At time 10 the queue would look like: SouthWest 25(Started at 10), United 147(Scheduled for 15)")
    print("At time 11 the queue would look like: SouthWest 25(Started at 10), United 147(Scheduled for 15)")
    print("At time 12 the queue would look like: SouthWest 25(Started at 10), United 147(Scheduled for 15)")
    print("At time 13 the queue would look like: SouthWest 25(Started at 10), United 147(Scheduled for 15)")
    print("At time 14 the queue would look like: SouthWest 25(Started at 10), United 147(Scheduled for 15)")
    print("At time 15 the queue would look like: United 147(Started at 15)")
    print("At time 16 the queue would look like: United 147(Started at 15)")
    print("At time 17 the queue would look like: United 147(Started at 15)")
    print("At time 18 the queue would look like: N/A - All Planes have flown")
    print("JetBlue 113 (1-3), United 125 (4-5), Alaska 179 (6-6), SouthWest 25 (10-14), United 147 (15-17)")
    print("\n")
    print("Output:")
    runway.simulate_airport(starttime)
    print("\n")


def test_main():
    test_one()
    test_two()
    test_three()
    test_four()
    test_five()


if __name__ == "__main__":
    test_main()