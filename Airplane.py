class Airplane:
    """
    Airplane.py
    Compilers 1

    This class is used to create an airplane object that will hold information about a given plane.

    Attributes:
        identity: A string that contains the identity of the plane in [Name Number] format.
        subtime: An int that holds the submission time of the plane.
        reqtime: An int that holds the requested time of the plane.
        reqduration: An int that holds the requested duration of the plane.
        starttime: An int that holds the time that the plane begins to fly.
        endtime: An int that holds the time the plane leaves the airport.

    author: Nghia Huynh
    date: October 11, 2018
    version: 2
    """
    def __init__(self):
        # Variables that are NOT mutable
        self.identity = ""
        self.subtime = 0
        self.reqtime = 0
        self.reqduration = 0
        # Variables that are mutable
        self.starttime = 0
        self.endtime = 0

    def __init__(self, identity, subtime, reqtime, reqduration):
        self.identity = identity
        self.subtime = subtime
        self.reqtime = reqtime
        self.reqduration = reqduration
        self.starttime = reqtime
        self.endtime = int(self.reqtime) + int(reqduration) - 1

# Variables that are NOT mutable
    def get_id(self):
        return str(self.identity)

    def get_sub_time(self):
        return int(self.subtime)

    def get_req_time(self):
        return int(self.reqtime)

    def get_req_duration(self):
        return int(self.reqduration)
# Variables that Are mutable

    def get_start_time(self):
        return int(self.starttime)

    def get_end_time(self):
        return int(self.endtime)

    def set_start_time(self, newtime):
        self.starttime = newtime

    def set_end_time(self, newtime):
        self.endtime = newtime

    def get_airplane(self):
        """
        This returns some of the variables of the Airplane object in a list to the caller.
        :return: A list of variables of the plane.
        """
        planeinfo = [self.identity, self.subtime, self.starttime, self.endtime]
        return planeinfo
