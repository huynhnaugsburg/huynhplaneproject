For iteration 1 I would like to do some more complete testing on my airport and make sure
all of the edge cases are taken care of. The main three files are Airport.py, Airplane.py,
and Runway.py. All other python code will be there for testing the main code only. 

This code functions well on the given plane values on moodle, and can even handle a list of planes that dont start at 0.
There are still some formatting of strings that need to be done for the second iteration. Also I want to start breaking up some
of the larger chunks of code such as the actual simulator because it currently has two different operation stages within itself.