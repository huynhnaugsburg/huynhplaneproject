import sys
import Runway


def main():
    """
    Airport.py
    Compilers 1

    This is the main that will be called to run the other classes for the Airport.

    author: Nghia Huynh
    date: October 11, 2018
    version: 2
    """
    runway = Runway.Runway()
    starttime = runway.insert_planes(sys.argv[1])
    runway.simulate_airport(starttime)


if __name__ == "__main__":
    main()


