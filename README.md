Airplane Program Ver1
This program will take in a given word document and generate a simulation of airplanes being flown from a airport.
----------------------
Note: Only three files are used to run the Aiport
Airport.py
Airplane.py
Runway.py

The fourth python file "PlaneMaker.py" should be ran by itself for testing purposes only. 
It is used to generate a Plane.txt file with random planes that can be used with the main code.
You can use TestMain.py to run the code without needing to input your own file because it uses PlaneMaker.py to make planes for you.
----------------------
Getting Started & Running:
1.	Open a Terminal window and path your way to the folder where Airplane.py exists.
2.	Choose a file name that will be used in .txt format and put it in the same folder as Airplane.py.
3.	Run the folling commands inside of the terminal window:
	
	python Airplane.py YOURTXTFILE.txt
	
	Note: Replace YOURTXTFILE.txt with the name of your .txt file.
4.	Look at terminal as the planes are generated.
----------------------
Built With:
Python- Used to code project

----------------------
Author:
Nghia Huynh - Code and Stuff - Augsburg University

----------------------
License:
This project is license under nobody and is free to use for all.

----------------------
Acknowledgements:
Google - Telling me what to do.

