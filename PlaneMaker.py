import random
"""
PlaneMaker.py
Compilers 1

Makes a Planes.txt filled with random planes

author: Nghia Huynh
date: October 11, 2018
version: 2
"""


def random_plane_name():
    plane = random.randint(1, 10)
    if plane == 1:
        return "Alaska"
    elif plane == 2:
        return "Allegiant"
    elif plane == 3:
        return "American"
    elif plane == 4:
        return "Delta"
    elif plane == 5:
        return "Frontier"
    elif plane == 6:
        return "Hawaiian"
    elif plane == 7:
        return "JetBlue"
    elif plane == 8:
        return "SouthWest"
    elif plane == 9:
        return "Spirit"
    elif plane == 10:
        return "United"


def plane_maker():
    file = open('Planes.txt', 'w')
    try:
        file.write("ID, Submission Time, Requested Start, Requested Duration\n")
        for x in range(0, 5):
            planenum = str(random.randint(1, 200))
            subtime = str(random.randint(0, 10))
            reqtime = str(random.randint(int(subtime), 10))
            reqduration = str(random.randint(1, 6))
            file.write(random_plane_name() + " " + planenum + ", " + subtime + ", " + reqtime + ", " + reqduration + "\n")
    except IOError:
            print("File could not be written to!")


def main():
    plane_maker()


if __name__ == "__main__":
    main()