import Airplane


class Runway:
    """
    Runway.py
    Compilers 1

    This class will be used to simulate an airport by adding planes to a runway and then sending them off when their
    requested flight time is reached.

    Attributes:
        runway: A list object that will act as a simulated runway.
        planepen: A list object that will hold all of the planes inside of itself.

    author: Nghia Huynh
    date: October 11, 2018
    version: 2
    """

    def __init__(self):
        self.runway = []
        self.planepen = []

    def insert_planes(self, inputfile):
        """
        This code takes in a file and gets all of the plane information and stores it within a plane pen.
        :param inputfile: This is the name of the input file.
        :return: firstsub: The value of the first earlier submission time.
        """
        file = open(inputfile, 'r')
        # This try catch block is used to check if the file that is entered can be read.
        try:
            # This for loop iterates through the file to read each line in one by one.
            for line in file:
                # Checks if first line of file is a specific string.
                if line.find("ID, Submission Time, Requested Start, Requested Duration") == -1 and line.strip():
                    # This part removes parts of the string, splits it and creates an airplane object to append runway.
                    line = line.replace('\n', '').split(", ")
                    # This if statement is used to check if the plane is a valid plane.
                    if int(line[1]) > int(line[2]):
                        print(line[0] + " not added! Not a valid plane! Request time is before submission time!")
                    else:
                        airplane = Airplane.Airplane(line[0], line[1], line[2], line[3])
                        self.planepen.append(airplane)
        except IOError:
            print("Could not read file: " + inputfile)
        # Sorts planes by their submission time in acceding order.
        # This will allow the clock to start when the actual first submissions is sent in. Which could not be 0.
        self.planepen.sort(key=lambda info: info.get_sub_time())
        firstsub = int(self.planepen[0].get_sub_time())
        return firstsub

    def check_runway(self, time):
        """
        This method takes in a given time and generates the output strings for what the runway would look like at that
        time.
        :param time: The time that runway is currently at.
        :return: Reruns a string of the runway state that can be printed to the terminal.
        """
        state = "At time " + str(time) + " the queue would look like: "
        # This is for when there are no planes on current runway.
        if not self.runway:
            state = state + "No planes taxiing!"
            return state
        # For loop to create the string for the plane output.
        for plane in self.runway:
            # If statement is used to format how the plane is printed depending on where it is located in the list.
            # Last Plane and if its the only plane.
            if plane == self.runway[-1]:
                # Checks if the plane is in the process of leaving or not and changes string accordingly.
                if plane.get_start_time() <= time:
                    state = state + plane.get_id() + "(Started at " + str(plane.get_start_time()) + ")"
                else:
                    state = state + plane.get_id() + "(Scheduled for " + str(plane.get_start_time()) + ")"
            # First Plane only
            elif plane == self.runway[0]:
                # Checks if the plane is in process of leaving or not and changes string accordingly.
                if plane.get_start_time() <= time:
                    state = state + plane.get_id() + "(Started at " + str(plane.get_start_time()) + "), "
                else:
                    state = state + plane.get_id() + "(Scheduled for " + str(plane.get_start_time()) + "), "
            # Planes in middle
            else:
                state = state + plane.get_id() + "(Scheduled for " + str(plane.get_start_time()) + "), "
        return state

    def add_submissions(self, time):
        """
        This takes in a given time and checks for planes that have that submission time inside of the planepen and
        then adds those planes to the end of the runway.
        :param time: The time that runway is currently at.
        :return: No value is returned.
        """
        for plane in self.planepen:
            if int(plane.get_sub_time()) == time:
                    self.runway.append(plane)

    def organize_planes(self):
        """
        This method sorts the runway list and puts the planes in accending order based on their request time and if they
        have similar request time then it orders them by submission time. Then the code will then use the first plane in
        the runway as a marker and begin pushing the times of planes back if they overlap with the plane before them.
        :return: No value is returned.
        """
        if self.runway:
            # Sorts plane by first request time, submission time, and then duration time.
            self.runway.sort(key=lambda info: (info.get_req_time(), info.get_sub_time(), info.get_req_duration()))
            # Grabs all planes but last one.
            for plane in self.runway[:-1]:
                # These lines get the index of current plane and the start and end time of the current and next plane.
                index = self.runway.index(plane)
                endtimefirstplane = self.runway[index].get_end_time()
                starttimesecondplane = self.runway[index + 1].get_start_time()
                endtimesecondplane = self.runway[index + 1].get_end_time()
                # Checks if the second plane start time overlap the first plane if so it pushes the 2nd plane over.
                if endtimefirstplane >= starttimesecondplane:
                    timedifferance = endtimefirstplane - starttimesecondplane + 1
                    self.runway[index + 1].set_start_time(starttimesecondplane + timedifferance)
                    self.runway[index + 1].set_end_time(endtimesecondplane + timedifferance)

    def simulate_airport(self, starttime):
        """
        This method is used to call the other internal methods so that the runway class can simulate an airport.
        :param starttime: This is the starting time of the earliest plane.
        :return: No value is returned.
        """
        # This list is used to hold the planes that have flown
        flownplanes = []
        # This variable is used to keep track of the time.
        time = starttime
        # This while loop iterates until all planes have flown by checking when runway is empty.
        while True:
            self.add_submissions(time)
            self.organize_planes()
            print(self.check_runway(time))
            # This checks if there are planes in the runway or not.
            if self.runway:
                # This checks the end time of current lead plane and sees if it has left the runway if so pop it.
                flytime = self.runway[0].get_end_time()
                if flytime == time:
                    flownplanes.append(self.runway.pop(0))
            time = time + 1
            # Breaks the while loop if the runway list is empty and if all planes in plane pen have flown.
            if not self.runway and len(flownplanes) == len(self.planepen):
                break
        # These two outputs just tell the user all planes have left by a certain time and begins to show the flights.
        print("At time " + str(time) + " the queue would look like: N/A - All Planes have flown")
        print("Here is a list of all the planes for today!")
        # Just an empty string that will appended to.
        planestring = ""
        # This iterates over the list of flown planes and sprints our their start and end times to the terminal.
        for plane in flownplanes:
            planestart = str(plane.get_start_time())
            planeend = str(plane.get_end_time())
            if plane == flownplanes[-1]:
                planestring = planestring + plane.get_id() + " (" + planestart + "-" + planeend + ")"
            else:
                planestring = planestring + plane.get_id() + " (" + planestart + "-" + planeend + "), "
        print(planestring)
